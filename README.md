# OpenRoadEd

OpenRoadEd is a simple application designed to create both logical (OpenDRIVE standard) and geometrical (OpenSceneGraph) descriptions of road networks.

Master's thesis: http://hdl.handle.net/2077/23047

Original authors of the software:
- Kurteanu, Dmitri
- Kurteanu, Egor

--------------

Project archive (including binaries and Visual Studio project files) can be found here: https://gitlab.com/OpenRoadEd/archive